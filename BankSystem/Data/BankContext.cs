﻿using BankSystem.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankSystem.Data
{
    public class BankContext : DbContext
    {
        public BankContext(DbContextOptions<BankContext> options)
            : base(options)
    {

    }
    public DbSet<Bank> Banks { get; set; }
    public DbSet<Account> Accounts { get; set; }
}
    {
    }
}
