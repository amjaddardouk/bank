﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace BankSystem.Models
{
    public class Bank
    {
        public int id { get; set; }
        public string name { get; set; }
        public ICollection<Account> Accounts { get; set; }
        public Bank()
        {
            Accounts = new Collection<Account>();
        }
    }
}
