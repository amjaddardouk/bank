﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BankSystem.Models
{
    public class Account
    {
        public int Id { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        [Required]
        [StringLength(255)]
        public string main_currency { get; set; }
        public ICollection<AccountType> AccountTypes { get; set; }
        public Account()
        {
            AccountTypes = new Collection<AccountType>();
        }
    }
}
