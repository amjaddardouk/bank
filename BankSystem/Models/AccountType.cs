﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankSystem.Models
{
    public class AccountType
    {
        public int id { get; set; }
        public string Type { get; set; }
        public int AccountId { get; set; }
        public string currancy_type { get; set; }
        public int amount { get; set; }

    }
}
