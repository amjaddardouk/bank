﻿using BankSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankSystem.Interfaces
{
   public interface IAccount
    {
        ICollection<Account> GetAcountList();
        Account GetAcCountById(int id);
        Account CreatAccount(string name, string mainCurrancy);
        void AddAcount(Account account);
        void UpdataAccount(int id);
        void DeleteAccount(int id);


    }
}
