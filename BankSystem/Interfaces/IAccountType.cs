﻿using BankSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankSystem.Interfaces
{
    interface IAccountType
    {
        ICollection<AccountType> GetAccountTypeList();
        AccountType GetAccountType();
        AccountType CreateAccountType(String type, int accountId, string currancyType, int amount);
        string GetCurrancyType();
        void SetCurrancyType(string type);
        int GetAccountAmount();
        void SetAccountAmount(int amount);
        




    }
}
