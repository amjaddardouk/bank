﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankSystem.Resources
{
    public class AccountTypeResorce
    {
        public int id { get; set; }
        public string Type { get; set; }
        public int AccountId { get; set; }
    }
}
