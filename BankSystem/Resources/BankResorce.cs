﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace BankSystem.Resources
{
    public class BankResorce
    {
        public int id { get; set; }
        public string name { get; set; }
        public ICollection<AccountResorce> AccountResorces { get; set; }
        public BankResorce()
        {
            AccountResorces = new Collection<AccountResorce>();
        }
    }
}
