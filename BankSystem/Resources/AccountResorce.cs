﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace BankSystem.Resources
{
    public class AccountResorce
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string type { get; set; }
        public string currency { get; set; }
        public ICollection<AccountTypeResorce> AccountTypeResorces { get; set; }
        public AccountResorce()
        {
            AccountTypeResorces = new Collection<AccountTypeResorce>();
        }
    }
}
